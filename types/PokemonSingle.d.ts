export type PokemonSingle = {
  name: string;
  weight: number;
  height: number;
  types: Array<string>;
  image: string;
  abilities: Array<string>;
  baseExperience: number;
  moves: Array<string>;
};
