export type PokemonSingleResponse = {
  name: string;
  weight: number;
  height: number;
  types: Array<{
    type: {
      name: string;
    };
  }>;
  sprites: {
    front_default: string;
  };
  abilities: Array<{
    ability: {
      name: string;
    };
  }>;
  base_experience: number;
  moves: Array<{
    move: {
      name: string;
    };
  }>;
};
