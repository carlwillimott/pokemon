const theme = {
  colours: {
    primary: "blue",
    secondary: "red",
    tertiary: "#F0F0F0",
    black: "black",
    white: "white",
  },
  spacing: {
    sm: "0.8rem",
    md: "1rem",
  },
};

export { theme };
