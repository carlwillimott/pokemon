import React from "react";
import { render, screen } from "@testing-library/react";
import { PokemonDetail } from "../../components/PokemonDetail";
import { PokemonSingle } from "../../types/PokemonSingle";

describe("<PokemonDetail />", () => {
  let props: PokemonSingle;

  beforeEach(() => {
    props = {
      abilities: ["ability 1", "ability 2"],
      baseExperience: 50,
      height: 10,
      image: "test.png",
      moves: ["move 1", "move 2"],
      name: "test",
      types: ["type 1", "type 2"],
      weight: 100,
    };
  });

  it("should render correctly", () => {
    const { asFragment } = render(<PokemonDetail {...props} />);

    expect(asFragment()).toMatchSnapshot();
  });

  it("should populate the pokemon image", () => {
    render(<PokemonDetail {...props} />);

    const image = screen.getByRole("img");
    expect(image).toHaveAttribute("src", "test.png");
    expect(image).toHaveAttribute("alt", "test");
  });

  it("should populate the pokemon height", () => {
    render(<PokemonDetail {...props} />);

    const height = screen.getByText("Height - 10");
    expect(height).toBeInTheDocument();
  });

  it("should populate the pokemon weight", () => {
    render(<PokemonDetail {...props} />);

    const weight = screen.getByText("Weight - 100");
    expect(weight).toBeInTheDocument();
  });

  it("should populate the pokemon types", () => {
    render(<PokemonDetail {...props} />);

    const types = screen.getByText("Types - type 1,type 2");
    expect(types).toBeInTheDocument();
  });

  it("should populate the pokemon abilities", () => {
    render(<PokemonDetail {...props} />);

    const abilities = screen.getByText("Abilities - ability 1,ability 2");
    expect(abilities).toBeInTheDocument();
  });

  it("should populate the pokemon experience", () => {
    render(<PokemonDetail {...props} />);

    const experience = screen.getByText("Base XP - 50");
    expect(experience).toBeInTheDocument();
  });

  it("should populate the pokemon moves", () => {
    render(<PokemonDetail {...props} />);

    const moves = screen.getByText("Moves - move 1,move 2");
    expect(moves).toBeInTheDocument();
  });
});
