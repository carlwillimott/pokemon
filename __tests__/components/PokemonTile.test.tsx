import React from "react";
import { render, screen } from "@testing-library/react";
import { PokemonTile } from "../../components/PokemonTile";
import { Pokemon } from "../../types/Pokemon";

describe("<PokemonTile />", () => {
  let props: Pokemon;

  beforeEach(() => {
    props = {
      name: "test",
      id: "1",
    };
  });

  it("should render correctly", () => {
    const { asFragment } = render(<PokemonTile {...props} />);

    expect(asFragment()).toMatchSnapshot();
  });

  it("should populate the pokemon link", () => {
    render(<PokemonTile {...props} />);

    const link = screen.getByRole("link");
    expect(link).toHaveAttribute("href", "/pokemon/1");
  });

  it("should populate the pokemon title", () => {
    render(<PokemonTile {...props} />);

    const link = screen.getByText("1. test");
    expect(link).toBeInTheDocument();
  });

  it("should populate the pokemon image", () => {
    render(<PokemonTile {...props} />);

    const image = screen.getByRole("img");
    expect(image).toHaveAttribute(
      "src",
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png"
    );
    expect(image).toHaveAttribute("alt", "test");
    expect(image).toHaveAttribute("loading", "lazy");
  });
});
