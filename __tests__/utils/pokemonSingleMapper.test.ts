import { pokemonSingleMapper } from "../../utils/pokemonSingleMapper";

describe("pokemonSingleMapper", () => {
  it("should be able to map a pokemon single response", () => {
    const response = {
      name: "test",
      weight: 100,
      height: 10,
      types: [
        {
          type: {
            name: "type 1",
          },
        },
        {
          type: {
            name: "type 2",
          },
        },
      ],
      sprites: {
        front_default: "test.png",
      },
      abilities: [
        {
          ability: {
            name: "ability 1",
          },
        },
        {
          ability: {
            name: "ability 2",
          },
        },
      ],
      base_experience: 50,
      moves: [
        {
          move: {
            name: "move 1",
          },
        },
        {
          move: {
            name: "move 2",
          },
        },
      ],
    };

    const expected = {
      abilities: ["ability 1", "ability 2"],
      baseExperience: 50,
      height: 10,
      image: "test.png",
      moves: ["move 1", "move 2"],
      name: "test",
      types: ["type 1", "type 2"],
      weight: 100,
    };

    expect(pokemonSingleMapper(response)).toEqual(expected);
  });
});
