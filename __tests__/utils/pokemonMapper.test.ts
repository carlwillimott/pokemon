import { extractId, pokemonMapper } from "../../utils/pokemonMapper";

describe("pokemonMapper", () => {
  describe("extractId", () => {
    it("should be able to extract an id", () => {
      expect(extractId("/pokemon/1/")).toEqual("1");
      expect(extractId("/pokemon/999/")).toEqual("999");
    });

    it("should return an empty string when unable to find an id", () => {
      expect(extractId("/pokemon//")).toEqual("");
      expect(extractId("")).toEqual("");
    });
  });

  describe("pokemonMapper", () => {
    it("should be able to map a pokemon response", () => {
      const response = [
        {
          name: "test1",
          url: "/pokemon/1/",
        },
        {
          name: "test2",
          url: "/pokemon/2/",
        },
      ];

      const expected = [
        {
          name: "test1",
          id: "1",
        },
        {
          name: "test2",
          id: "2",
        },
      ];

      expect(pokemonMapper(response)).toEqual(expected);
    });
  });
});
