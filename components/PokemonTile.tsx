import React from "react";
import Link from "next/link";
import { Pokemon } from "../types/Pokemon";
import { theme } from "../styles/theme";

const PokemonTile = ({ name, id }: Pokemon) => {
  const src = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`;

  return (
    <div className="pokemon-tile">
      <a className="pokemon-tile__link" href={`/pokemon/${id}`}>
        <p className="pokemon-tile__name">{`${id}. ${name}`}</p>
        <img
          className="pokemon-tile__image"
          src={src}
          alt={name}
          loading="lazy"
        />
      </a>
      <style jsx>{`
        .pokemon-tile {
          max-width: 300px;
          margin: 0 auto ${theme.spacing.md} auto;
        }

        .pokemon-tile__link {
          display: block;
          padding: ${theme.spacing.md};
          border: 1px dashed ${theme.colours.black};
          border-radius: 25px;
        }

        .pokemon-tile__link:hover {
          background: ${theme.colours.tertiary};
        }

        .pokemon-tile__name {
          margin: 0;
        }
      `}</style>
    </div>
  );
};

export { PokemonTile };
