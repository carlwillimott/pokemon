import React from "react";
import { PokemonSingle } from "../types/PokemonSingle";
import { theme } from "../styles/theme";

const PokemonDetail = ({
  image,
  name,
  height,
  weight,
  types,
  abilities,
  baseExperience,
  moves,
}: PokemonSingle) => {
  return (
    <div className="pokemon-detail">
      <img className="pokemon-detail__image" src={image} alt={name} />
      <p className="pokemon-detail__height">{`Height - ${height}`}</p>
      <p className="pokemon-detail__weight">{`Weight - ${weight}`}</p>
      <p className="pokemon-detail__types">{`Types - ${types}`}</p>
      <p className="pokemon-detail__abilities">{`Abilities - ${abilities}`}</p>
      <p className="pokemon-detail__base-experience">{`Base XP - ${baseExperience}`}</p>
      <p className="pokemon-detail__moves">{`Moves - ${moves}`}</p>
      <style jsx>{`
        .pokemon-detail {
          max-width: 300px;
          margin: 0 auto ${theme.spacing.md} auto;
        }

        .pokemon-detail__image {
          margin: ${theme.spacing.md} 0;
        }
      `}</style>
    </div>
  );
};

export { PokemonDetail };
