import { PokemonSingleResponse } from "../types/PokemonSingleResponse";
import { PokemonSingle } from "../types/PokemonSingle";

const pokemonSingleMapper = ({
  name,
  weight,
  height,
  types,
  sprites,
  abilities,
  base_experience,
  moves,
}: PokemonSingleResponse): PokemonSingle => ({
  name,
  weight,
  height,
  types: types.map(({ type: { name } }) => name),
  image: sprites.front_default,
  abilities: abilities.map(({ ability: { name } }) => name),
  baseExperience: base_experience,
  moves: moves.map(({ move: { name } }) => name),
});

export { pokemonSingleMapper };
