import { PokemonResponse } from "../types/PokemonResponse";
import { Pokemon } from "../types/Pokemon";

const extractId = (url: string) => {
  const matches = url.match(/pokemon\/(\d+)/);
  return matches !== null ? matches[1] : "";
};

const pokemonMapper = (results: Array<PokemonResponse>): Array<Pokemon> =>
  results.map(({ name, url }: PokemonResponse) => ({
    name,
    id: extractId(url),
  }));

export { extractId, pokemonMapper };
