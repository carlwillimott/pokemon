import type { GetServerSidePropsContext, NextPage } from "next";
import Head from "next/head";
import { PokemonTile } from "../components/PokemonTile";
import { PropsWithChildren } from "react";
import { Pokemon } from "../types/Pokemon";
import { pokemonMapper } from "../utils/pokemonMapper";
import { theme } from "../styles/theme";

type HomeProps = PropsWithChildren<any> & {
  results?: Array<Pokemon>;
};

const Home: NextPage = ({ results }: HomeProps) => {
  const title = "Home";
  const intro = "Please select a pokemon";

  return (
    <div>
      <Head>
        <title>{title}</title>
        <meta name="description" content={title} />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="index-page">
        <h1 className="index-page__title">{title}</h1>
        <h2 className="index-page__intro">{intro}</h2>
        <div className="index-page__details">
          {results.map((pokemon: Pokemon, index: string) => (
            <PokemonTile {...pokemon} key={index} />
          ))}
        </div>
      </main>
      <style jsx>{`
        .index-page {
          text-align: center;
          margin: ${theme.spacing.md};
        }

        .index-page__title {
          margin-top: 0;
          color: ${theme.colours.primary};
        }

        .index-page__intro {
          margin-top: 0;
          color: ${theme.colours.secondary};
        }
      `}</style>
    </div>
  );
};

export async function getServerSideProps({ res }: GetServerSidePropsContext) {
  res.setHeader(
    "Cache-Control",
    "public, s-maxage=10, stale-while-revalidate=59"
  );

  const limit = 151;

  const response = await fetch(
    `https://pokeapi.co/api/v2/pokemon?limit=${limit}`
  );
  const json = await response.json();

  return { props: { results: pokemonMapper(json.results) } };
}

export default Home;
