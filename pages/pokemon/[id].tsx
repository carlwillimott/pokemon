import type { GetServerSidePropsContext, NextPage } from "next";
import Head from "next/head";
import Link from "next/link";
import { PropsWithChildren } from "react";
import { pokemonSingleMapper } from "../../utils/pokemonSingleMapper";
import { PokemonDetail } from "../../components/PokemonDetail";
import { PokemonSingle } from "../../types/PokemonSingle";
import { theme } from "../../styles/theme";

type PokemonProps = PropsWithChildren<any> & {
  result: PokemonSingle;
};

const Pokemon: NextPage = ({ result }: PokemonProps) => {
  const title = `Pokemon - ${result.name}`;

  return (
    <div>
      <Head>
        <title>{title}</title>
        <meta name="description" content={title} />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="pokemon-page">
        <h1 className="pokemon-page__title">{title}</h1>
        <Link href="/">
          <a className="pokemon-page__back">Back</a>
        </Link>
        <PokemonDetail {...result} />
      </main>
      <style jsx>{`
        .pokemon-page {
          text-align: center;
          margin: ${theme.spacing.md};
          overflow-wrap: break-word;
        }

        .pokemon-page__title {
          margin-top: 0;
          color: ${theme.colours.primary};
        }

        .pokemon-page__back {
          border: 1px solid ${theme.colours.black};
          padding: ${theme.spacing.sm};
          display: inline-block;
        }

        .pokemon-page__back:hover {
          background-color: ${theme.colours.tertiary};
        }
      `}</style>
    </div>
  );
};

export async function getServerSideProps({
  res,
  params,
}: GetServerSidePropsContext) {
  res.setHeader(
    "Cache-Control",
    "public, s-maxage=10, stale-while-revalidate=59"
  );

  const id = params?.id;

  const response = await fetch(`https://pokeapi.co/api/v2/pokemon/${id}`);
  const json = await response.json();

  return { props: { result: pokemonSingleMapper(json) } };
}

export default Pokemon;
