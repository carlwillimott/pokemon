## Getting Started
Please use the following commands to get started:
```bash
npm install
npm run dev
npm run test
```

## Overview
The following application is built with Next.js, and uses server-side rendering to display information about the first 151 pokemon.
I've spent a minimal amount of time on this application as I wanted to convey a breath of skills rather than focusing too heavily on one particular aspect.
I will now discuss the approach I took and the reasons why:
* `Server-side rendering` - I felt that this was a good approach to take, and meant I was able to fetch and transform the information on the server. I just pass the `id` in the url and use this to load the individual pokemon. I've added some caching headers in each of the `getServerSideProps` requests, but edge caching would be required on the server for this to take effect.
* `Separate pages and components` - One conscious choice I have made is to extract much of the page content into separate components. This is to make things easier to test in isolation and also keeps things more structured and easy to follow.
* `Static-typing` - I have used typescipt and created my own custom typings to aid with the development process and hopefully catch bugs in development instead of production.
* `Fetching all results at once` - Instead of adding extra complexity with pagination or a lazy-loader, I opted to fetch and render all 151 pokemon at once. I even cheated and used a url with an interpolated `id` extracted from the resource location to display an image. I did however, add a lazy loading attribute to the images in order to defer loading.
* `Error handling` - I specifically kept this simple and made the assumption that we would have a happy-path on each page load. For a production release we would need to handle this. In fact, you might want to even implement a `useEffect` to make a request to the server to fetch the results and display a loading spinner or something.
* `Testing` - I have implemented a range of different unit tests including snapshots for components. You will notice that I typically only test happy paths as I wanted to keep things simple. I am aware that API requests could fail, but chose to ignore that fact.
* `Syling` - I left this until the end, and have only included a small amount to illustrate the approach I would take. I am a fan of `styled-jsx` and think that it can be useful to have a custom `useTheme` hook or something similar to provide consistency with variables such as colours, sizing and spacing etc. In the past I've used a theme HOC, but find that hooks are more desirable.
* `Git` - I like to follow conventional commits and encourage other developers to do so also for consistency. I would normally create a feature branch and then merge a PR into master.

## Issues
* There is an issue in the `PokemonTile` component which I haven't had chance to resolve. I had to revert to using an `anchor` instead of the next `Link` component because of an error (it's probably something obscure).